/*
 * MessageRouter.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the logic for routing ISA messages.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueue;
import com.domenix.utils.IPCQueueEvent;
import com.domenix.utils.IPCQueueEventType;
import com.domenix.utils.IPCQueueListenerInterface;
import com.google.common.collect.ImmutableList;
import gov.isa.model.*;
import gov.isa.net.MetaData;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Contains the logic for routing received messages to the correct processor based on sensor type.
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class MessageRouter implements Runnable , IPCQueueListenerInterface
{

  /**
   * Error/Debug Logger
   */
  private final Logger MY_LOGGER = LogManager.getLogger( JCADReportProcessor.class );
  /**
   * The control queue
   */
  private final IPCQueue<JHRMRecorderCtlQueueEntry> CTL_QUEUE;
  /**
   * The JCAD data queue.
   */
  private final IPCQueue<JCADDataQueueEntry> JCAD_QUEUE;
  /**
   * The Hapsite data queue.
   */
  private final IPCQueue<HapsiteDataQueueEntry> HAPSITE_QUEUE;
  /**
   * The MultiRAE data queue.
   */
  private final IPCQueue<MultiRAEDataQueueEntry> MULTIRAE_QUEUE;
  /**
   * The Message Router data queue.
   */
  private final IPCQueue<RouterDataQueueEntry> ROUTER_QUEUE;
  /**
   * Flag indicating we should shut down.
   */
  private static final AtomicBoolean SHUTDOWN_FLAG = new AtomicBoolean( false );
  /**
   * The display frame
   */
  private final JHRMRecorderFrame theFrame = JHRMRecorder.getMY_FRAME();
  /**
   * The message information.
   */
  private Message theMsg;
  private Header theHdr;
  private MetaData theMeta;

  /**
   * Constructor
   */
  public MessageRouter()
  {
    CTL_QUEUE = JHRMRecorder.getCTL_QUEUE();
    JCAD_QUEUE = JHRMRecorder.getJCAD_QUEUE();
    HAPSITE_QUEUE = JHRMRecorder.getHAPSITE_QUEUE();
    MULTIRAE_QUEUE = JHRMRecorder.getMULTIRAE_QUEUE();
    ROUTER_QUEUE = JHRMRecorder.getROUTER_QUEUE();

    MY_LOGGER.warn( "Message router constructed." );
  }

  /**
   * Handle queue entries.
   *
   * @param evt the IPC queue event.
   */
  @Override
  public void queueEventFired( IPCQueueEvent evt )
  {
    if ( evt.getEventType() == IPCQueueEventType.IPC_SHUTDOWN )
    {
      MessageRouter.setSHUTDOWN_FLAG( true );
    }
    else
    {
      RouterDataQueueEntry entry = this.ROUTER_QUEUE.readFirst( 0L );
      if ( entry != null )
      {
        KnownInfoList infoList = this.getKnownSensorInfoList();
        switch ( evt.getEventType() )
        {
          case IPC_SHUTDOWN:
            MessageRouter.setSHUTDOWN_FLAG( true );
            break;

          case IPC_NEW:
            theMsg = entry.getTheMsg();
            theHdr = entry.getTheHdr();
            theMeta = entry.getTheMeta();
            theFrame.incrementRecvdCount();
            if ( theMsg != null )
            {
              String kind = "Admin";
              TreeSet<String> types = new TreeSet<>();
              try
              {
                ArrayList<String> aboutUs = new ArrayList<>();
                boolean known = false;
                if ( theMsg instanceof Administrative )
                {
                  MY_LOGGER.debug( "Raw Msg: " + ((Administrative)theMsg).toString() );
                  ImmutableList<UCI> about = ( (Administrative) theMsg ).getAbout();
                  for ( UCI x : about )
                  {
                    String work = x.uci();
                    String work2 = work.toUpperCase().trim();
                    for ( KnownInfo y : infoList.theList )
                    {
                      if ( ( work2.toUpperCase().contains( y.name ) ) && ( y.enabled ) &&
                           ( !work2.toUpperCase().contains( "REPLAY" ) ) )
                      {
                        aboutUs.add( work );
                        types.add( y.name );
                      }
                    }
                    if ( aboutUs.isEmpty() )
                    {
                      return;
                    }
                    known = true;
                  }
                }
                else
                {
                  String theSensor = theMsg.getSource().uci();

                  if ( theMsg instanceof Config )
                  {
                    kind = "Config";
                  }
                  else if ( theMsg instanceof Event )
                  {
                    kind = "Event";
                  }
                  else if ( theMsg instanceof Status )
                  {
                    kind = "Status";
                  }
                  MY_LOGGER.debug( "Received: " + theSensor + " Type: " + kind );

                  for ( KnownInfo y : infoList.theList )
                  {
                    if ( ( theSensor.contains( y.name ) ) && ( y.enabled ) &&
                         ( !theSensor.toUpperCase().contains( "Replay" ) ) )
                    {
                      known = true;
                      types.add( y.name );
                    }
                  }
                }
                if ( known )
                {
                  types.forEach( (z) ->
                  {
                    if ( z.equalsIgnoreCase( KnownSensorType.JCAD.name() ) )
                    {
                      this.JCAD_QUEUE.insertLast( new JCADDataQueueEntry( IPCQueueEventType.IPC_NEW , theMsg , theHdr ,
                                                                          theMeta ) );
                    }
                    else if ( z.equalsIgnoreCase( KnownSensorType.HAPSITE.name() ) )
                    {
                      this.HAPSITE_QUEUE.insertLast( new HapsiteDataQueueEntry( IPCQueueEventType.IPC_NEW , theMsg ,
                                                                                theHdr , theMeta ) );
                    }
                    else if ( z.equalsIgnoreCase( KnownSensorType.MULTIRAE.name() ) )
                    {
                      this.MULTIRAE_QUEUE.insertLast( new MultiRAEDataQueueEntry( IPCQueueEventType.IPC_NEW , theMsg ,
                                                                                  theHdr , theMeta ) );
                    }
                  } );
                }
              }
              catch ( IllegalStateException ex )
              {
                MY_LOGGER.error( "Exception routing message." , ex );
              }
            }
            
          default :
            break;
        }
      }
    }
  }

  /**
   * Returns known sensor entry information for this application.
   *
   * @return KnownInfoList of the sensor data
   */
  private KnownInfoList getKnownSensorInfoList()
  {
    KnownInfoList retValue = new KnownInfoList();

    for ( int i = 0; i < ( JHRMRecorder.getKNOWN_TYPES().length ); i++ )
    {
      KnownInfo entry = new KnownInfo( JHRMRecorder.getKNOWN_TYPES()[ i ] , JHRMRecorder.getKNOWN_ENABLED()[ i ] );
      retValue.theList.add( entry );
    }
    return ( retValue );
  }

  /**
   * Sensor known information
   */
  class KnownInfo
  {

    /**
     * The sensor type name
     */
    String name;
    /**
     * Is the sensor enabled?
     */
    boolean enabled;

    /**
     * Constructor
     *
     * @param name    the sensor type name
     * @param enabled the sensor type enable flag
     */
    public KnownInfo( String name , Boolean enabled )
    {
      this.name = name;
      this.enabled = enabled;
    }
  }

  /**
   * A list of KnowInfo records
   */
  class KnownInfoList
  {

    /**
     * The list of entries
     */
    ArrayList<KnownInfo> theList = new ArrayList<>();

    /**
     * Constructor
     */
    public KnownInfoList()
    {
    }
  }

  /**
   * Run this thread.
   */
  @Override
  public void run()
  {
    CTL_QUEUE.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    ROUTER_QUEUE.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    ROUTER_QUEUE.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    boolean doMore = true;
    try
    {
      while ( ( doMore ) && ( !MessageRouter.getSHUTDOWN_FLAG() ) )
      {
        try
        {
          Thread.sleep( 500L );
        }
        catch ( InterruptedException intEx )
        {
          doMore = false;
          MessageRouter.setSHUTDOWN_FLAG( true );
        }
      }
    }
    catch ( Exception ex )
    {
      MY_LOGGER.error( "Exception waiting for shutdown" , ex );
    }
    CTL_QUEUE.removeIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    ROUTER_QUEUE.removeIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    ROUTER_QUEUE.removeIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
  }

  /**
   * @return the SHUTDOWN_FLAG
   */
  public static boolean getSHUTDOWN_FLAG()
  {
    return SHUTDOWN_FLAG.get();
  }

  /**
   * @param aSHUTDOWN_FLAG the SHUTDOWN_FLAG to set
   */
  public static void setSHUTDOWN_FLAG( boolean aSHUTDOWN_FLAG )
  {
    SHUTDOWN_FLAG.set( aSHUTDOWN_FLAG );
  }

}
