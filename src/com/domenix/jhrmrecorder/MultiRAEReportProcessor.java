/*
 * MultiRAEReportProcessor.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the code for processing received messages from a MultiRAE
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.utils.IPCQueue;
import com.domenix.utils.IPCQueueEvent;
import com.domenix.utils.IPCQueueEventType;
import com.domenix.utils.IPCQueueListenerInterface;
import com.domenix.utils.PrettyPrintJSON;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import gov.isa.model.AdminCode;
import gov.isa.model.Administrative;
import gov.isa.model.BoundedRange;
import gov.isa.model.CapabilityDeclaration;
import gov.isa.model.ChemicalMaterialClass;
import gov.isa.model.ChemicalReading;
import gov.isa.model.CommandDeclaration;
import gov.isa.model.CommandState;
import gov.isa.model.Config;
import gov.isa.model.CustomType;
import gov.isa.model.CustomTypeDeclaration;
import gov.isa.model.DiscreteRange;
import gov.isa.model.Event;
import gov.isa.model.EventType;
import gov.isa.model.FieldDeclaration;
import gov.isa.model.GeographicPosition;
import gov.isa.model.Header;
import gov.isa.model.MeasuredKgpm3;
import gov.isa.model.MeasuredPpm;
import gov.isa.model.Message;
import gov.isa.model.NameValuePair;
import gov.isa.model.ObservableDeclaration;
import gov.isa.model.ObservableState;
import gov.isa.model.ParameterDeclaration;
import gov.isa.model.Percent;
import gov.isa.model.PropertyDeclaration;
import gov.isa.model.PropertyState;
import gov.isa.model.Range;
import gov.isa.model.RangeDeclaration;
import gov.isa.model.ResultDeclaration;
import gov.isa.model.Secs;
import gov.isa.model.StandardIdentity;
import gov.isa.model.Status;
import gov.isa.model.Structure;
import gov.isa.model.Type;
import gov.isa.model.UCI;
import gov.isa.model.UTC;
import gov.isa.net.MetaData;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Extract information from a MultiRAE related message and write it to the output.
 *
 * @author thomas.swanson
 * @version 1.0
 */
public class MultiRAEReportProcessor implements Runnable , IPCQueueListenerInterface
{

  /**
   * Error/Debug Logger
   */
  private final Logger MY_LOGGER = LogManager.getLogger( MultiRAEReportProcessor.class );

  /**
   * The input queue.
   */
  private final IPCQueue<MultiRAEDataQueueEntry> theQueue;

  /**
   * Flag indicating we should shut down.
   */
  private static final AtomicBoolean SHUTDOWN_FLAG = new AtomicBoolean( false );

  /**
   * The output file.
   */
  private final BufferedWriter outFile;

  /**
   * The display frame.
   */
  private final JHRMRecorderFrame theFrame;

  /**
   * Pretty print JSON
   */
  private final PrettyPrintJSON jsonPP = new PrettyPrintJSON();

  /**
   * The message information.
   */
  private Message theMsg;
  private Header theHdr;
  private MetaData theMeta;

  /**
   * The constructor
   *
   * @param outFile the logging output file
   */
  public MultiRAEReportProcessor( BufferedWriter outFile )
  {
    theQueue = JHRMRecorder.getMULTIRAE_QUEUE();
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    this.outFile = outFile;
    this.theFrame = JHRMRecorder.getMY_FRAME();
    MY_LOGGER.info( "Report processor constructed." );
  }

  /**
   * Set the shutdown flag
   *
   * @param val the value to be set
   */
  public static void setSHUTDOWN_FLAG( boolean val )
  {
    MultiRAEReportProcessor.SHUTDOWN_FLAG.set( val );
  }

  /**
   * @return the SHUTDOWN_FLAG
   */
  public static boolean getSHUTDOWN_FLAG()
  {
    return ( SHUTDOWN_FLAG.get() );
  }

  /**
   * Queue entry received.
   *
   * @param evt the queue event
   */
  @Override
  public void queueEventFired( IPCQueueEvent evt )
  {
    MultiRAEDataQueueEntry entry = this.theQueue.readFirst( 0L );
    if ( entry != null )
    {
      switch ( evt.getEventType() )
      {
        case IPC_SHUTDOWN:
          MultiRAEReportProcessor.setSHUTDOWN_FLAG( true );
          break;

        case IPC_NEW:
          theMsg = entry.getTheMsg();
          theHdr = entry.getTheHdr();
          theMeta = entry.getTheMeta();
          if ( theMsg != null )
          {
            boolean retValue = this.processMessage();
            if ( retValue )
            {
              MY_LOGGER.info( "Successfully processed message: " + theMsg.getIdentifier().toString() );
            }
            else
            {
              MY_LOGGER.error( "Failed to process message: " + theMsg.getIdentifier().toString() );
            }
          }
          break;

        default:
          MY_LOGGER.info( "Received unhandled queue entry type " + evt.getEventType().name() );
          break;
      }
    }
  }

  /**
   * Process the specific message received.
   *
   * @return the processing status success/failure
   */
  private boolean processMessage()
  {
    boolean retValue = false;
    KnownInfoList infoList = this.getKnownSensorInfoList();
    String theSensor = theMsg.getSource().uci();
    UTC reportTimeUTC = theMsg.getTime();
    double reportTime = reportTimeUTC.getValue().atomicValue();
    long identifier = theMsg.getIdentifier();
    if ( theMsg instanceof Administrative )
    {
      ArrayList<String> aboutUs = new ArrayList<>();
      ImmutableList<UCI> about = ( (Administrative) theMsg ).getAbout();
      for ( UCI x : about )
      {
        String y = x.uci().trim().toUpperCase();
        if ( ( y.contains( "MULTIRAE" ) ) & ( !y.contains( "REPLAY" ) ) )
        {
          aboutUs.add( y );
        }
      }
      theFrame.incrementAdminCount();
      retValue = dumpAdminMessage( theMsg , theSensor , identifier , reportTime , aboutUs );
    }
    else if ( theMsg instanceof Event )
    {
      theFrame.incrementEventCount();
      retValue = this.dumpEventMessage( theMsg , theSensor , identifier , reportTime );
    }
    else if ( theMsg instanceof Config )
    {
      theFrame.incrementConfigCount();
      retValue = this.dumpConfigMessage( theMsg , theSensor , identifier , reportTime );
    }
    else if ( theMsg instanceof Status )
    {
      theFrame.incrementStatusCount();
      retValue = this.dumpStatusMessage( theMsg , theSensor , identifier , reportTime );
    }
    else
    {
      retValue = true;  // Ignore things we don't care about
    }
    return ( retValue );
  }

  /**
   * Run the thread
   */
  @Override
  public void run()
  {
    /**
     * Thread execution.
     */
//    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_NEW );
//    theQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    boolean doMore = true;
    try
    {
      while ( ( doMore ) && ( !MultiRAEReportProcessor.getSHUTDOWN_FLAG() ) )
      {
        try
        {
          Thread.sleep( 500L );
        }
        catch ( InterruptedException intEx )
        {
          doMore = false;
          JCADReportProcessor.setSHUTDOWN_FLAG( true );
          MY_LOGGER.error( "Thread interrupted for JCAD" , intEx );
        }
      }
    }
    catch ( Exception ex )
    {
      MY_LOGGER.error( "Exception waiting for shutdown" , ex );
    }
  }

  /**
   * Process an event message.
   *
   * @param msg        the message
   * @param theSensor  the sensor UCI
   * @param identifier the message identifier
   * @param reportTime the time of the report
   *
   * @return the status of the result
   */
  private boolean dumpEventMessage( Message msg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;
    try
    {
      Event event = (Event) msg;
      EventType.Predefined eventType = event.getType().predef();
      String logMsg = "Processing Event " + eventType.name() + " message from: " + theSensor;
      MY_LOGGER.info( logMsg );
      MY_LOGGER.debug( "Raw Message: " + event.toString() );
      this.updateDisplay( logMsg );
      JSONObject finalObj = new JSONObject();
      JSONObject eventObj = new JSONObject();
      eventObj.put( "source" , theSensor );
      eventObj.put( "identifier" , identifier );
      long priority = 80L;
      if ( event.getPriority().isPresent() )
      {
        priority = event.getPriority().get().atomicValue();
      }
      eventObj.put( "priority" , priority );
      eventObj.put( "time" , reportTime );
      long eventId = event.getEventID().event_id();
      eventObj.put( "event ID" , eventId );
      if ( event.getEventReference().isPresent() )
      {
        JSONObject evtRefObj = new JSONObject();
        String refuci = event.getEventReference().get().getCreator().uci();
        long refevt = event.getEventReference().get().getId().event_id();
        double reftim = event.getEventReference().get().getTime().getValue().secs();
        evtRefObj.put( "creator" , refuci );
        evtRefObj.put( "id" , refevt );
        evtRefObj.put( "time" , reftim );
        eventObj.put( "event reference" , evtRefObj );
      }
      eventObj.put( "type" , eventType.name() );
      switch ( eventType )
      {
        case ALARM:
        case ALERT:
        case DETECTION:
        case MEASUREMENT:
        {
          ImmutableList<NameValuePair> observables = event.getObservables();
          if ( observables.size() > 0 )
          {
            JSONObject obsObj = new JSONObject();
            for ( NameValuePair x : observables )
            {
              if ( x.getName().equalsIgnoreCase( "Chemical Reading" ) )
              {
                JSONObject chemReading = new JSONObject();
                ChemicalReading cReading = (ChemicalReading) x.getValue();
                if ( cReading.getMaterialClass().isPresent() )
                {
                  chemReading.put( "material class" , cReading.getMaterialClass().get().value() );
                }
                else
                {
                  chemReading.put( "material class" , "UNKNOWN" );
                }
                if ( cReading.getMaterialName().isPresent() )
                {
                  chemReading.put( "material name" , cReading.getMaterialName().get() );
                }
                else
                {
                  chemReading.put( "material name" , "None" );
                }
                if ( cReading.getServiceNumber().isPresent() )
                {
                  chemReading.put( "service number" , cReading.getServiceNumber().get() );
                }
                else
                {
                  chemReading.put( "service number" , "None" );
                }
                if ( cReading.getHarmful().isPresent() )
                {
                  chemReading.put( "harmful" , cReading.getHarmful().get() );
                }
                else
                {
                  chemReading.put( "harmful" , true );
                }
                if ( cReading.getDensity().isPresent() )
                {
                  chemReading.put( "density" , cReading.getDensity().get().getValue().kgpm3() );
                }
                else
                {
                  chemReading.put( "density" , 0.0 );
                }
                if ( cReading.getMassFraction().isPresent() )
                {
                  chemReading.put( "massFraction" , cReading.getMassFraction().get().getValue().ppm() );
                }
                else
                {
                  chemReading.put( "massFraction" , 0.0 );
                }

                obsObj.put( "Chemical Reading" , chemReading );
              }
              else if ( ( x.getName().equalsIgnoreCase( "Chemical Level" ) )
                || ( x.getName().equalsIgnoreCase( "Chemical Reading Level" ) ) )
              {
                String useName = x.getName().trim();
                JSONObject chemLevel = new JSONObject();
                if ( x.getValue() != null )
                {
                  ImmutableList<NameValuePair> lvlObj = ( (CustomType) x.getValue() ).getFields();
                  if ( !lvlObj.isEmpty() )
                  {
                    for ( NameValuePair anObj : lvlObj.asList() )
                    {
                      ImmutableList<NameValuePair> parts = ( ( CustomType ) anObj.getValue() ).getFields();
                      for ( NameValuePair y : parts )
                      {
                        if ( y.getName().equalsIgnoreCase( "material name" ) )
                        {
                          chemLevel.put( "material name" , (String) y.getValue() );
                        }
                        else if ( y.getName().equalsIgnoreCase( "material class" ) )
                        {
                          chemLevel.put( "material class" , ((ChemicalMaterialClass)y.getValue()).value() );
                        }
                        else if ( y.getName().equalsIgnoreCase( "service number" ) )
                        {
                          chemLevel.put( "service number" , (String) y.getValue() );
                        }
                        else if ( y.getName().equalsIgnoreCase( "harmful" ) )
                        {
                          chemLevel.put( "harmful" , (Boolean)y.getValue() );
                        }
                        else if ( y.getName().equalsIgnoreCase( "level" ) )
                        {
                          ImmutableList<NameValuePair> l = ( (CustomType) y.getValue() ).getFields();
                          if ( ( l != null ) && ( !l.isEmpty() ) )
                          {
                            JSONObject aLvl = new JSONObject();
                            for ( NameValuePair z : l.asList() )
                            {
                              aLvl.put( "level" , (Double) z.getValue() );
                              aLvl.put( "name" , (String) z.getName() );
                            }
                            chemLevel.put( "level" , aLvl );
                          }
                        }
                        else if ( y.getName().equalsIgnoreCase( "mass fraction" ) )
                        {
                          chemLevel.put( "mass fraction" , ((MeasuredPpm) y.getValue()).getValue().atomicValue() );
                        }
                        else if ( y.getName().equalsIgnoreCase( "concentration" ) )
                        {
                          chemLevel.put( "concentration" , ((MeasuredKgpm3) y.getValue()).getValue().atomicValue() );
                        }
                        else if ( y.getName().equalsIgnoreCase( "confidence" ) )
                        {
                          chemLevel.put( "confidence" , ((Percent) y.getValue()).percent() );
                        }
                        else
                        {
                          MY_LOGGER.info( "Unknown element in level: " + y.getName() );
                        }
                      }
                    }
                  }
                }
                obsObj.put( useName , chemLevel );
              }
              else if ( x.getName().equalsIgnoreCase( "AdditionalMeasurements" ) )
              {
                JSONObject addMeasObj = new JSONObject();
                ImmutableList<NameValuePair> detProp = ( (CustomType) x.getValue() ).getFields();
                if ( ( detProp != null ) && ( !detProp.isEmpty() ) )
                {
                  for ( NameValuePair p : detProp )
                  {
                    if ( p.getName().equalsIgnoreCase( "units" ) )
                    {
                      addMeasObj.put( "units" , ( (String) p.getValue() ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "twa" ) )
                    {
                      addMeasObj.put( "twa" , ( ( (Double) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "stel" ) )
                    {
                      addMeasObj.put( "stel" , ( ( (Double) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "highAlarmActive" ) )
                    {
                      addMeasObj.put( "highAlarmActive" , ( ( (Boolean) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "lowAlarmActive" ) )
                    {
                      addMeasObj.put( "lowAlarmActive" , ( ( (Boolean) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "stelAlarmActive" ) )
                    {
                      addMeasObj.put( "stelAlarmActive" , ( ( (Boolean) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "twaAlarmActive" ) )
                    {
                      addMeasObj.put( "twaAlarmActive" , ( ( (Boolean) p.getValue() ) ) );
                    }
                    else if ( p.getName().equalsIgnoreCase( "driftActive" ) )
                    {
                      addMeasObj.put( "driftAction" , ( ( (Boolean) p.getValue() ) ) );
                    }
                    else
                    {
                      MY_LOGGER.info( "Unknown element in additional measurements: " + p.getName() );
                    }
                  }
                  obsObj.put( "AdditionalMeasurements" , addMeasObj );
                }
              }
              else if ( x.getName().equalsIgnoreCase( "Position" ) )
              {
                JSONObject posObj = new JSONObject();
                GeographicPosition pos = (GeographicPosition) ( x.getValue() );
                posObj.put( "latitude" , pos.getLatitude().degrees() );
                posObj.put( "longitude" , pos.getLatitude().degrees() );
                if ( pos.getAltitude().isPresent() )
                {
                  posObj.put( "altitude" , pos.getAltitude().get().meters() );
                }
                else
                {
                  posObj.put( "altitude" , 0.0 );
                }
                obsObj.put( "Position" , posObj );
              }
            }
            eventObj.put( "observables" , obsObj );  // end Observables
          }

          ImmutableList<NameValuePair> properties = event.getDetectorProperties();
          if ( !properties.isEmpty() )
          {
            JSONObject detPropObj = new JSONObject();
            for ( NameValuePair x : event.getDetectorProperties() )
            {
              detPropObj.put( x.getName() , x.getValue().toString() );
            }
            eventObj.put( "detector properties" , detPropObj );// end detector properties
          }
          finalObj.put( "Event" , eventObj );
          try
          {
            MY_LOGGER.info( this.jsonPP.prettyPrintJSON( finalObj.toString() ) );
            this.outFile.write( finalObj.toString() );
            this.outFile.newLine();
            this.outFile.flush();
            theFrame.incrementLoggedCount();
            retValue = true;
            MY_LOGGER.info( "Wrote new event object for " + theSensor );
          }
          catch ( IOException ioEx )
          {
            MY_LOGGER.error( "Exception writing Event message to the output file" , ioEx );
            this.updateDisplay( "Exception writing Event message to the output file: " + ioEx.getLocalizedMessage() );
            retValue = false;
          }
        }
      }
    }
    catch ( IllegalStateException | JSONException ovrEx )
    {
      MY_LOGGER.error( "Exception processing Event message to the output file" , ovrEx );
      this.updateDisplay( "Exception processing Event message to the output file: " + ovrEx.getLocalizedMessage() );
      retValue = false;
    }
    return ( retValue );
  }

  /**
   * Process and output a Status message
   *
   * @param theMsg     the received message
   * @param theSensor  the UCI of the sensor
   * @param identifier the identifier of the message
   * @param reportTime the time of the report
   *
   * @return success/failure
   */
  private boolean dumpStatusMessage( Message theMsg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;
    try
    {
      MY_LOGGER.info( "Processing Status message for: " + theSensor );
      this.updateDisplay( "Processing Status message for: " + theSensor );
      Status staMsg = (Status) theMsg;
      MY_LOGGER.debug( "Raw Message: " + staMsg.toString() );
      JSONObject finalObj = new JSONObject();
      JSONObject statusObj = new JSONObject();
      long priority = 80L;
      try
      {
        if ( staMsg.getPriority().isPresent() )
        {
          priority = staMsg.getPriority().get().atomicValue();
        }
        statusObj.put( "source" , theSensor );
        statusObj.put( "identifier" , identifier );
        statusObj.put( "priority" , priority );
        statusObj.put( "time" , reportTime );
        ImmutableList<NameValuePair> properties = staMsg.getProperties();
        if ( ( properties != null ) && ( !properties.isEmpty() ) )
        {
          JSONObject propertiesObj = new JSONObject();
          for ( NameValuePair base : properties )
          {
            if ( base.getName().equalsIgnoreCase( "AdditionalProperties" ) )
            {
              JSONObject addPropObj = new JSONObject();
              ImmutableList<NameValuePair> addProp = ( (CustomType) base.getValue() ).getFields();
              for ( NameValuePair y : addProp )
              {
                if ( y.getName().equalsIgnoreCase( "sensorType" ) )
                {
                  addPropObj.put( "sensorType" , ( (String) y.getValue() ).trim() );
                }
                else if ( y.getName().equalsIgnoreCase( "firmwareVersion" ) )
                {
                  addPropObj.put( "firmwareVersion" , (String) y.getValue() );
                }
                else if ( y.getName().equalsIgnoreCase( "battery_low" ) )
                {
                  addPropObj.put( "battery_low" , ( (Boolean) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "power_normal" ) )
                {
                  addPropObj.put( "power_normal" , ( (Boolean) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "pump_stall" ) )
                {
                  addPropObj.put( "pump_stall" , ( (Boolean) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "memory_full" ) )
                {
                  addPropObj.put( "memory_full" , ( (Boolean) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "sensorAlarm" ) )
                {
                  addPropObj.put( "sensorAlarm" , ( (Boolean) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "unitFailure" ) )
                {
                  addPropObj.put( "unitFailure" , ( (Boolean) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "alarmMode" ) )
                {
                  addPropObj.put( "alarmMode" , ( (Boolean) y.getValue() ) );
                }
                else if ( y.getName().equalsIgnoreCase( "sensorComponents" ) )
                {
                  JSONArray compObj = new JSONArray();
                  ImmutableList<NameValuePair> compList = ( (CustomType) y.getValue() ).getFields();
                  if ( compList != null )
                  {
                    for ( NameValuePair aComp : compList )
                    {
                      JSONObject compInst = new JSONObject();
                      if ( aComp.getName().equalsIgnoreCase( "sensorComponent" ) )
                      {
                        ImmutableList<NameValuePair> compDat = ( (CustomType) aComp.getValue() ).getFields();
                        for ( NameValuePair c : compDat )
                        {
                          if ( c.getName().equalsIgnoreCase( "sensorComponentType" ) )
                          {
                            compInst.put( "sensorComponentType" , ( (String) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "service number" ) )
                          {
                            compInst.put( "service number" , ( (String) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "lowAlarm" ) )
                          {
                            compInst.put( "lowAlarm" , ( (Double) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "highAlarm" ) )
                          {
                            compInst.put( "highAlarm" , ( (Double) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "stelAlarm" ) )
                          {
                            compInst.put( "stelAlarm" , ( (Double) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "twaAlarm" ) )
                          {
                            compInst.put( "twaAlarm" , ( (Double) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "overRange" ) )
                          {
                            compInst.put( "overRange" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "maxSaturated" ) )
                          {
                            compInst.put( "maxSaturated" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "failed" ) )
                          {
                            compInst.put( "failed" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "highAlarmActive" ) )
                          {
                            compInst.put( "highAlarmActive" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "lowAlarmActive" ) )
                          {
                            compInst.put( "lowAlarmActive" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "stelAlarmActive" ) )
                          {
                            compInst.put( "stelAlarmActive" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "twaAlarmActive" ) )
                          {
                            compInst.put( "twaAlarmActive" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "driftActive" ) )
                          {
                            compInst.put( "driftActive" , ( (Boolean) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "max" ) )
                          {
                            compInst.put( "max" , ( (Double) c.getValue() ) );
                          }
                          else if ( c.getName().equalsIgnoreCase( "min" ) )
                          {
                            compInst.put( "min" , ( (Double) c.getValue() ) );
                          }
                        }
                        compObj.put( compInst );
                      }
                    }
                  }
                  addPropObj.put( "sensorComponents" , compObj );
                }
                propertiesObj.put( "AdditionalProperties" , addPropObj );
              }
            }  // end loop additional props
            else if ( base.getName().equalsIgnoreCase( "Position" ) )
            {
              JSONObject posObj = new JSONObject();
              GeographicPosition pos = (GeographicPosition) ( base.getValue() );
              posObj.put( "latitude" , Double.toString( pos.getLatitude().degrees() ) );
              posObj.put( "longitude" , Double.toString( pos.getLongitude().degrees() ) );
              if ( pos.getAltitude().isPresent() )
              {
                posObj.put( "altitude" , Double.toString( pos.getAltitude().get().meters() ) );
              }
              propertiesObj.put( "Position" , posObj );
            }
            else if ( base.getName().equalsIgnoreCase( "Manufacturer" ) )
            {
              propertiesObj.put( "Manufacturer" , ( (String) base.getValue() ) );
            }
            else if ( base.getName().equalsIgnoreCase( "Identity" ) )
            {
              propertiesObj.put( "Identity" , ( (StandardIdentity) base.getValue() ).getSymbol().sidc() );
            }
            else if ( base.getName().equalsIgnoreCase( "Minimum Status Interval" ) )
            {
              propertiesObj.put( "Minimum Status Interval" , ( (Secs) base.getValue() ).secs() );
            }
            else if ( base.getName().equalsIgnoreCase( "Maximum Status Interval" ) )
            {
              propertiesObj.put( "Maximum Status Interval" , ( (Secs) base.getValue() ).secs() );
            }
            else if ( base.getName().equalsIgnoreCase( "Serial Number" ) )
            {
              propertiesObj.put( "Serial Number" , ( (String) base.getValue() ).trim() );
            }
            else if ( base.getName().equalsIgnoreCase( "Model" ) )
            {
              propertiesObj.put( "Model" , ( (String) base.getValue() ).trim() );
            }
            else
            {
              MY_LOGGER.info( "Found property: " + base.getName() + " => " + base.getValue().toString() );
            }
          }
          statusObj.put( "properties" , propertiesObj );
        }
        retValue = true;
        finalObj.put( "Status" , statusObj );
      }
      catch ( NumberFormatException | JSONException ex )
      {
        MY_LOGGER.error( "Exception creating properties object." , ex );
        retValue = false;
      }
      try
      {
        MY_LOGGER.info( this.jsonPP.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        this.outFile.flush();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Status message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Status message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing received message" , ex );
      updateDisplay( "*** Exception processing received message" + ex.getLocalizedMessage() );
      retValue = false;
    }
    return ( retValue );
  }

  /**
   * Logs the content of a configuration message for a sensor.
   *
   * @param msg        the Configuration message
   * @param theSensor  the sensor UCI
   * @param identifier the message ID
   * @param reportTime the message time
   *
   * @return success/failure
   */
  private boolean dumpConfigMessage( Message msg , String theSensor , long identifier , double reportTime )
  {
    boolean retValue = false;
    try
    {
      MY_LOGGER.info( "Processing Config message for: " + theSensor );
      this.updateDisplay( "Processing Config message for: " + theSensor );
      Config config = (Config) msg;
      MY_LOGGER.debug( "Raw Message: " + config.toString() );
      JSONObject finalObj = new JSONObject();
      try
      {
        JSONObject cfgObj = new JSONObject();
        cfgObj.put( "source" , theSensor );
        cfgObj.put( "identifier" , identifier );
        long priority = 80L;
        if ( config.getPriority().isPresent() )
        {
          priority = config.getPriority().get().atomicValue();
        }
        cfgObj.put( "priority" , priority );
        cfgObj.put( "time" , reportTime );

        if ( config.getCcd().isPresent() )
        {
          JSONObject ccdObj = new JSONObject();
          Optional<CapabilityDeclaration> ccd = config.getCcd();
          ImmutableList<PropertyDeclaration> properties = ccd.get().getProperties();
          if ( properties != null )
          {
            JSONArray propertiesObj = new JSONArray();
            for ( PropertyDeclaration x : properties )
            {
              JSONObject propDecl = new JSONObject();
              propDecl.put( "name" , x.getName() );
              propDecl.put( "description" , x.getDescription() );
              propDecl.put( "type" , ( (Type) x.getType() ).value() );
              propDecl.put( "mutability" , x.getMutability().value() );
              propDecl.put( "structure" , ( (Structure) x.getStructure() ) );
              if ( x.getRange().isPresent() )
              {
                JSONArray rangeDecl = new JSONArray();
                Optional<RangeDeclaration> rd = x.getRange();

                JSONArray ranges = new JSONArray();
                for ( Range r : rd.get().getRanges() )
                {
                  JSONObject range = this.processRange( r );
                  ranges.put( range );
                }
                rangeDecl.put( ranges );
                propDecl.put( "range" , rangeDecl );
              }
              if ( x.getThreshold().isPresent() )
              {
                propDecl.put( "threshold" , x.getThreshold().toString() );
              }
              propertiesObj.put( propDecl );
            }
            ccdObj.put( "properties" , propertiesObj );
          }
          ImmutableList<CommandDeclaration> commands = ccd.get().getCommands();
          if ( commands != null )
          {
            JSONArray cmdObj = new JSONArray();
            for ( CommandDeclaration x : commands.asList() )
            {
              JSONObject cmdDecl = new JSONObject();
              cmdDecl.put( "name" , x.getName() );
              ImmutableList<ParameterDeclaration> args = x.getArgs();
              if ( ( args != null ) && ( !args.isEmpty() ) )
              {
                JSONArray params = new JSONArray();
                for ( ParameterDeclaration y : args.asList() )
                {
                  JSONObject paramDecl = new JSONObject();
                  paramDecl.put( "name" , y.getName() );
                  paramDecl.put( "description" , y.getDescription() );
                  paramDecl.put( "type" , ( (Type) y.getType() ).value() );
                  paramDecl.put( "optional" , y.getOptional() );
                  paramDecl.put( "structure" , ( (Structure) y.getStructure() ).toString() );
                  if ( y.getRange().isPresent() )
                  {
                    JSONObject rangeDecl = new JSONObject();
                    Optional<RangeDeclaration> rd = y.getRange();

                    JSONArray ranges = new JSONArray();
                    for ( Range r : rd.get().getRanges() )
                    {
                      JSONObject range = this.processRange( r );
                      ranges.put( range );
                    }
                    rangeDecl.put( "ranges" , ranges );
                  }
                  params.put( paramDecl );
                }
                cmdDecl.put( "args" , params );  // end args
              }
              if ( x.getDescription().isPresent() )
              {
                cmdDecl.put( "description" , x.getDescription().get() );
              }
              ImmutableList<ResultDeclaration> reslt = x.getResults();
              if ( ( reslt != null ) && ( !reslt.isEmpty() ) )
              {
                JSONArray results = new JSONArray();
                for ( ResultDeclaration z : reslt.asList() )
                {
                  JSONObject resltDecl = new JSONObject();
                  resltDecl.put( "name" , z.getName() );
                  resltDecl.put( "description" , z.getDescription() );
                  resltDecl.put( "type" , ( (Type) z.getType() ).value() );
                  resltDecl.put( "structure" , ( (Structure) z.getStructure() ).toString() );
                  resltDecl.put( "optional" , z.getOptional() );
                  if ( z.getRange().isPresent() )
                  {
                    Optional<Range> rd = z.getRange();

                    JSONObject range = this.processRange( (Range) rd );
                    resltDecl.put( "range" , range );
                  }
                  results.put( resltDecl );
                }
                cmdDecl.put( "Result Declaration" , results );  // results
              }
              cmdObj.put( cmdDecl );
            }
            ccdObj.put( "commands" , commands );  // commands
          }

          ImmutableList<ObservableDeclaration> observables = ccd.get().getObservables();
          if ( ( observables != null ) && ( !observables.isEmpty() ) )
          {
            JSONArray obsObj = new JSONArray();
            for ( ObservableDeclaration x : observables.asList() )
            {
              JSONObject obsDecl = new JSONObject();
              obsDecl.put( "name" , x.getName() );
              obsDecl.put( "description" , x.getDescription() );
              obsDecl.put( "type" , ( (Type) x.getType() ).value() );
              obsDecl.put( "structure" , ( (Structure) x.getStructure() ).toString() );
              if ( x.getRange().isPresent() )
              {
                JSONArray rangeDecl = new JSONArray();
                Optional<RangeDeclaration> rd = x.getRange();

                JSONArray ranges = new JSONArray();
                for ( Range r : rd.get().getRanges() )
                {
                  JSONObject range = this.processRange( r );
                  ranges.put( range );
                }
                rangeDecl.put( ranges );
                obsDecl.put( "range" , rangeDecl );
              }
              obsObj.put( obsDecl );  // observables
            }
            ccdObj.put( "observables" , obsObj );
          }

          ImmutableList<CustomTypeDeclaration> types = ccd.get().getTypes();
          if ( ( types != null ) && ( !types.isEmpty() ) )
          {
            JSONArray typesObj = new JSONArray();
            for ( CustomTypeDeclaration x : types )
            {
              JSONArray custom = new JSONArray();

              JSONObject oneCustom = new JSONObject();
              oneCustom.put( "name" , x.getName() );
              oneCustom.put( "description" , x.getDescription() );
              if ( !x.getFields().isEmpty() )
              {
                JSONArray fieldsObj = new JSONArray();
                ImmutableList<FieldDeclaration> fields = x.getFields();
                for ( FieldDeclaration y : fields )
                {
                  JSONObject fldDecl = new JSONObject();
                  fldDecl.put( "name" , y.getName() );
                  fldDecl.put( "description" , y.getDescription() );
                  fldDecl.put( "type" , ( (Type) y.getType() ).value() );
                  fldDecl.put( "structure" , ( (Structure) y.getStructure() ).toString() );
                  fldDecl.put( "optional" , y.getOptional() );
                  fieldsObj.put( fldDecl );
                }
                oneCustom.put( "fields" , fieldsObj );
              }
              custom.put( oneCustom );
              typesObj.put( custom ); // custom type decl
            }
            ccdObj.put( "types" , typesObj ); // types
          }
          cfgObj.put( "ccd" , ccdObj );  // ccd
        }

        ImmutableList<PropertyState> propStates = config.getPropertyStates();
        if ( ( propStates != null ) && ( !propStates.isEmpty() ) )
        {
          JSONArray propStatesObj = new JSONArray();
          for ( PropertyState x : propStates )
          {
            JSONObject pState = new JSONObject();
            pState.put( "name" , x.getName() );
            pState.put( "ready" , x.getReady() );
            pState.put( "reporting" , x.getReporting() );
            propStatesObj.put( pState );  // prop state
          }
          cfgObj.put( "property states" , propStatesObj );  // property states
        }

        ImmutableList<ObservableState> observables = config.getObservableStates();
        if ( ( observables != null ) && ( !observables.isEmpty() ) )
        {
          JSONArray obsStateObj = new JSONArray();
          for ( ObservableState x : observables )
          {
            JSONObject obsState = new JSONObject();
            obsState.put( "name" , x.getName() );
            obsState.put( "ready" , x.getReady() );
            obsState.put( "reporting" , x.getReporting() );
            obsStateObj.put( obsStateObj );
          }
          cfgObj.put( "observable states" , obsStateObj );  // observables
        }

        ImmutableList<CommandState> cmds = config.getCommandStates();
        if ( ( cmds != null ) && ( !cmds.isEmpty() ) )
        {
          JSONArray cmdStateObj = new JSONArray();
          for ( CommandState x : cmds )
          {
            JSONObject cmdState = new JSONObject();
            cmdState.put( "name" , x.getName() );
            cmdState.put( "ready" , x.getReady() );
            cmdStateObj.put( cmdState );  // command state
          }
          cfgObj.put( "command states" , cmdStateObj );  // command states
        }

        ImmutableList<NameValuePair> extras = config.getExtras();
        if ( ( extras != null ) && ( !extras.isEmpty() ) )
        {
          JSONArray extrasObj = new JSONArray();
          for ( NameValuePair x : extras )
          {
            JSONObject nvPairObj = new JSONObject();
            nvPairObj.put( x.getName() , x.getValue().toString() );
            extrasObj.put( nvPairObj );
          }
          cfgObj.put( "extras" , extrasObj );  // extras
        }
        finalObj.put( "Config" , cfgObj );
        retValue = true;
      }
      catch ( IllegalStateException | JSONException exObj )
      {
        MY_LOGGER.error( "Exception building configuration." , exObj );
        retValue = false;
      }
      try
      {
        MY_LOGGER.info( this.jsonPP.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        this.outFile.flush();
        theFrame.incrementLoggedCount();
        retValue = true;
        MY_LOGGER.info( "Successfully output config for: " + theSensor );
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Config message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Config message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing Config message." , ex );
    }
    return ( retValue );
  }

  /**
   * Returns a JSON Range object or null if an exception occurs.
   *
   * @param r the range to process
   *
   * @return the JSON or null
   */
  private JSONObject processRange( Range r )
  {
    JSONObject retValue = new JSONObject();
    try
    {
      if ( r instanceof BoundedRange )
      {
        JSONObject bRange = new JSONObject();
        bRange.put( "high inclusive" , ( (BoundedRange) r ).getHighInclusive() );
        bRange.put( "low inclusive" , ( (BoundedRange) r ).getLowInclusive() );
        if ( r.getDescription().isPresent() )
        {
          bRange.put( "description" , r.getDescription().get() );
        }
        bRange.put( "high" , ( (BoundedRange) r ).getHigh().toString() );
        bRange.put( "low" , ( (BoundedRange) r ).getLow().toString() );
        retValue.put( "BoundedRange" , bRange );
      }
      else if ( r instanceof DiscreteRange )
      {
        JSONObject dRange = new JSONObject();
        if ( r.getDescription().isPresent() )
        {
          dRange.put( "description" , r.getDescription().get() );
        }
        if ( ( (DiscreteRange) r ).getInclusive().isPresent() )
        {
          dRange.put( "inclusive" , ( (DiscreteRange) r ).getInclusive().get().toString() );
        }
        dRange.put( "value" , ( (DiscreteRange) r ).getValue().toString() );
        retValue.put( "DiscreteRange" , dRange );
      }
    }
    catch ( JSONException ex )
    {
      MY_LOGGER.error( "Exception processing a Range" , ex );
      retValue = null;
    }
    return ( retValue );
  }

  /**
   * Writes a received Admin message to the log.
   *
   * @param theMsg     the input Admin message
   * @param theSensor  the originating sensor
   * @param identifier the identifier
   * @param reportTime the report time
   * @param aboutUs    the list of known sensors
   *
   * @return flag indicating success/failure
   */
  private boolean dumpAdminMessage( Message theMsg , String theSensor , long identifier , double reportTime ,
    ArrayList<String> aboutUs )
  {
    boolean retValue;
    try
    {
      Administrative admin = (Administrative) theMsg;
      MY_LOGGER.debug( "Raw Message: " + admin.toString() );
      AdminCode code = admin.getCode();
      String codeStr = code.value();
      JSONObject finalObj = new JSONObject();
      JSONObject anObj = new JSONObject();
      //
      // Ignore types we don't process
      //
      if ( ( codeStr.contains( "BANDWIDTH" ) )
        || ( codeStr.contains( "FOLLOW" ) )
        || ( codeStr.contains( "SUBSCRIPT" ) )
        || ( codeStr.contains( "OTHER" ) ) )
      {
        return ( true );
      }

      try
      {
        anObj.put( "source" , theSensor );
        anObj.put( "identifier" , identifier );
        anObj.put( "time" , reportTime );
        if ( !aboutUs.isEmpty() )
        {
          anObj.put( "about" , aboutUs );
        }
        else
        {
          anObj.put( "about" , "[]" );
        }
        anObj.put( "code" , code.value() );
        finalObj.put( "Administrative" , anObj );
      }
      catch ( JSONException ex )
      {
        MY_LOGGER.error( "Exception building Admin message data." , ex );
        retValue = true;
        return ( retValue );
      }

      if ( code.value().equalsIgnoreCase( "DEVICE_ADDED" ) )
      {
        for ( String x : aboutUs )
        {
          updateDisplay( x + " has joined the network." );
          theFrame.incrementSensorCount();
        }
      }
      else if ( code.value().equals( "DEVICE_REMOVED" ) )
      {
        for ( String x : aboutUs )
        {
          updateDisplay( x + " has left the network." );
          theFrame.decrementSensorCount();
        }
      }
      else if ( code.value().equals( "DEVICE_NON_RESPONSIVE" ) )
      {
        for ( String x : aboutUs )
        {
          updateDisplay( x + " is not responding on the network." );
          theFrame.decrementSensorCount();
        }
      }
      else if ( code.value().equals( "DEVICE_REVIVED" ) )
      {
        for ( String x : aboutUs )
        {
          updateDisplay( x + " is again responding on the network." );
          theFrame.incrementSensorCount();
        }
      }
      try
      {
        MY_LOGGER.info( this.jsonPP.prettyPrintJSON( finalObj.toString() ) );
        this.outFile.write( finalObj.toString() );
        this.outFile.newLine();
        this.outFile.flush();
        theFrame.incrementLoggedCount();
        retValue = true;
      }
      catch ( IOException ioEx )
      {
        MY_LOGGER.error( "Exception writing Admin message to the output file" , ioEx );
        this.updateDisplay( "*** Exception writing Admin message to the output file: " + ioEx.getLocalizedMessage() );
        retValue = false;
      }
    }
    catch ( IllegalStateException ex )
    {
      MY_LOGGER.error( "Exception processing Admin message" , ex );
      retValue = false;
    }
    return ( retValue );
  }

  /**
   * Write a line to the display area.
   *
   * @param msg the message to be written
   */
  private void updateDisplay( String msg )
  {
    theFrame.writeToViewingArea( msg );
  }

  /**
   * Returns known sensor entry information for this application.
   *
   * @return KnownInfoList of the sensor data
   */
  private KnownInfoList getKnownSensorInfoList()
  {
    KnownInfoList retValue = new KnownInfoList();

    for ( int i = 0; i < ( JHRMRecorder.getKNOWN_TYPES().length ); i++ )
    {
      KnownInfo entry = new KnownInfo( JHRMRecorder.getKNOWN_TYPES()[ i ] , JHRMRecorder.getKNOWN_ENABLED()[ i ] );
      retValue.theList.add( entry );
    }
    return ( retValue );

  }

  /**
   * Sensor known information
   */
  class KnownInfo
  {

    /**
     * The sensor type name
     */
    String name;
    /**
     * Is the sensor enabled?
     */
    boolean enabled;

    /**
     * Constructor
     *
     * @param name    the sensor type name
     * @param enabled the sensor type enable flag
     */
    public KnownInfo( String name , Boolean enabled )
    {
      this.name = name;
      this.enabled = enabled;
    }
  }

  /**
   * A list of KnowInfo records
   */
  class KnownInfoList
  {

    /**
     * The list of entries
     */
    ArrayList<KnownInfo> theList = new ArrayList<>();

    /**
     * Constructor
     */
    public KnownInfoList()
    {
    }
  }

}
