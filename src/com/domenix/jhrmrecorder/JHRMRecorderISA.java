/*
 * JHRMRecorderISA.java - 20/2/22
 *
 * Copyright (c) 2020 Domenix Corp.
 *
 * This file contains the ISA client functions.
 *
 * Author: T. Swanson
 * Version: V1.0
 */
package com.domenix.jhrmrecorder;

import com.domenix.hpaisa.HPAISASecurity;
import com.domenix.utils.IPCQueue;
import com.domenix.utils.IPCQueueEvent;
import com.domenix.utils.IPCQueueEventType;
import com.domenix.utils.IPCQueueListenerInterface;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import com.google.common.net.InetAddresses;
import com.google.common.util.concurrent.ListenableFuture;
import gov.isa.behaviors.AbstractComponentBehavior;
import gov.isa.behaviors.BehaviorControls;
import gov.isa.behaviors.ComponentBehavior;
import gov.isa.behaviors.SubscriptionManager;
import gov.isa.components.PrimaryComponent;
import gov.isa.components.PrimaryComponentDeclaration;
import gov.isa.components.StandardComponentAdapter;
import gov.isa.model.DataQuery;
import gov.isa.model.Header;
import gov.isa.model.Message;
import gov.isa.model.ModelFactory;
import gov.isa.model.SubscriptionID;
import gov.isa.model.UCI;
import gov.isa.net.ConnectionMonitor;
import gov.isa.net.EndpointDescriptor;
import gov.isa.net.LifeCyclePhase;
import gov.isa.net.MetaData;
import gov.isa.spi.ComponentFactory;
import gov.isa.spi.ComponentFactoryException;
import gov.isa.spi.ComponentFactoryManager;
import gov.isa.spi.ComponentFactoryOption;
import gov.isa.spi.ComponentFactoryProvider;
import gov.isa.spi.DataQueryParser;
import gov.isa.spi.DataQueryParserException;
import gov.isa.spi.DataQueryParserManager;
import gov.isa.spi.DataQueryParserProvider;
import gov.isa.spi.StandardComponentFactoryOptions;
import java.io.BufferedWriter;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.swing.SwingUtilities;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * The ISA interface primary class
 *
 * @author thomas.swanson
 * @version 0.1
 */
public class JHRMRecorderISA implements Runnable , IPCQueueListenerInterface
{

  /**
   * Error/Debug logger
   */
  private final Logger MY_LOGGER;
  /**
   * The program's properties
   */
  private final Properties theProperties = JHRMRecorder.getCLIENT_PROPERTIES();
  /**
   * The component adapter
   */
  private StandardComponentAdapter standardComponentAdapter;
  /**
   * The model factory
   */
  private ModelFactory model_factory;
  /**
   * The UCI of this component
   */
  private final String my_uci;
  /**
   * The UCI of the controller
   */
  private final String controllerUCI;
  /**
   * The control IPC queue
   */
  private final IPCQueue<JHRMRecorderCtlQueueEntry> ctlQueue = JHRMRecorder.getCTL_QUEUE();
  /**
   * The data IPC queues
   */
 
  private final IPCQueue<RouterDataQueueEntry> routerQueue = JHRMRecorder.getROUTER_QUEUE();
  /**
   * The behavioral controls
   */
  private BehaviorControls behaviorControls;
  /**
   * The controller IP address
   */
  private final String controller_ip;
  /**
   * The controller port number
   */
  private final String controller_port;
  /**
   * The query to be used when register our subscription with the controller.
   */
  private final String query;
  /**
   * The GUI frame
   */
  private final JHRMRecorderFrame theFrame;
  /**
   * The output log file
   */
  private final BufferedWriter outFile;
  /**
   * The flag for shutting down processing.
   */
  private final AtomicBoolean shutdownFlag = new AtomicBoolean( false );
  /**
   * Flag indicating that the client is connected
   */
  private final AtomicBoolean client_connected = new AtomicBoolean( false );
  /**
   * The ISA primary component
   */
  PrimaryComponent client_component;
  /**
   * The server end point
   */
  EndpointDescriptor server_endpoint;
  /**
   * The component factory
   */
  ComponentFactory component_factory;
  /**
   * Subscription Manager
   */
  SubscriptionManager sub_manager;
  /**
   * Subscription ID
   */
  ListenableFuture<SubscriptionID> subscriptionID;

  /**
   * Constructor
   *
   * @param outWriter the writer for the data log
   */
  public JHRMRecorderISA( BufferedWriter outWriter )
  {
    MY_LOGGER = LogManager.getLogger( JHRMRecorderISA.class );
    MY_LOGGER.info( "Constructing ISA Interface" );
    theFrame = JHRMRecorder.getMY_FRAME();
    my_uci = theProperties.getProperty( "jhrm.recorder.self.uci" );
    controllerUCI = theProperties.getProperty( "jhrm.recorder.controller.uci" );
    controller_ip = theProperties.getProperty( "jhrm.recorder.controller.ip" );
    controller_port = theProperties.getProperty( "jhrm.recorder.controller.port" );
    query = theProperties.getProperty( "jhrm.recorder.query" , "select all" );
    MY_LOGGER.info( "My UCI: " + my_uci + " Cntrlr UCI: " + controllerUCI + " Cntrlr IP: " +
                    controller_ip + " Cntrlr Port: " + controller_port + " Qry: " + query );
    outFile = outWriter;
  }

  /**
   * React to shutdown queue events.
   *
   * @param evt the event generated
   */
  @Override
  public void queueEventFired( IPCQueueEvent evt )
  {
    if ( evt.getEventType() == IPCQueueEventType.IPC_SHUTDOWN )
    {
      JHRMRecorderCtlQueueEntry content = ctlQueue.readFirst( 1000L );
      MY_LOGGER.info( "Received queue entry " + content.getEventType().name() );
      SwingUtilities.invokeLater( () ->
      {
        MY_LOGGER.info( "Shutting down and disconnecting." );
        try
        {
          this.shutdownFlag.set( true );
        }
        catch ( Exception ex )
        {
          MY_LOGGER.error( "Exception shutting down the thread subscription." , ex );
        }
      } );
    }
  }

  /**
   * Shut down the ISA thread
   *
   * @param theThread the ISA thread to shut down
   *
   * @throws java.util.concurrent.ExecutionException if an exception in execution occurs
   * @throws java.lang.InterruptedException          if shutdown is interrupted
   */
  public void shutdown( Thread theThread ) throws InterruptedException , ExecutionException
  {
    if ( theThread.isAlive() )
    {
      MY_LOGGER.info( "Stopping the ISA interface" );
      addText( "Disconnecting..." );
      ctlQueue.removeIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
      this.shutdownFlag.set( true );
      ListenableFuture<ImmutableSet<SubscriptionID>> x = sub_manager.getActiveSubscriptions();
      ImmutableSet<SubscriptionID> y = x.get();
      if ( y.size() > 0 )
      {
        y.forEach( (z) ->
        {
          sub_manager.removeSubscription( z );
        } );
      }
      client_component.removeServerAtEndpoint( server_endpoint );
      client_component.removeClientToEndpoint( server_endpoint );
      component_factory.getBehavioralScheduler().shutdown();
      component_factory.getNetworkingScheduler().shutdown();
      
      this.client_connected.set( false );
      theFrame.setConnActValue( "Disconnected" );
      theFrame.writeToStatus( "Disconnected" );
      addText( "Disconnected." );
      theThread.interrupt();
    }
  }

  /**
   * Thread run method
   */
  @Override
  public void run()
  {
    MY_LOGGER.info( "ISA interface running" );
    theFrame.clearAllCounts();
    addText( "Connecting to ISA controller..." );
    ctlQueue.addIPCQueueEventListener( this , IPCQueueEventType.IPC_SHUTDOWN );
    this.client_connected.set( false );
    //Get a component factory manager
    ComponentFactoryManager manager = new ComponentFactoryManager();

    // Get the default isa api provider from the component factory manager
    ComponentFactoryProvider provider = manager.getDefaultProvider();

    try
    {
      HPAISASecurity isaSecurity = new HPAISASecurity();
      // create a KeyStoreRetriever
      StandardComponentFactoryOptions.KeyStoreRetriever keystore_retriever = isaSecurity.createKeystoreRetriever(
        Paths.get( System.getProperty( "user.home" ) , "/JHRMRecorder/keys" ) );
      // create a KeyPasswordRetriever
      StandardComponentFactoryOptions.KeyPasswordRetriever key_password_retriever = isaSecurity.createPasswordRetriever(
        Paths.get( System.getProperty( "user.home" ) , "/JHRMRecorder/keys" ) );
      // set initial min and max status intervals
      long initial_minimum_status_interval = 20L;
      long initial_maximum_status_interval = 60L;
      // Initialize the isa api provider and get the component factory
      // and
      // Initialize the provider with custom keystore options
      component_factory = provider.initialize(
        ImmutableSet.<ComponentFactoryOption<?>>of(
          new StandardComponentFactoryOptions.ComponentKeyStoreRetriever( keystore_retriever ) ,
          new StandardComponentFactoryOptions.ComponentKeyPasswordRetriever( key_password_retriever ) ,
          new StandardComponentFactoryOptions.ComponentTrustStoreRetriever( keystore_retriever ) ,
          StandardComponentFactoryOptions.InitialMinimumStatusInterval.create(
            initial_minimum_status_interval ,
            TimeUnit.SECONDS
          ) ,
          StandardComponentFactoryOptions.InitialMaximumStatusInterval.create(
            initial_maximum_status_interval ,
            TimeUnit.SECONDS
          )
        )
      );
      // Get a model factory from the component factory.  It is used to construct all data types
      model_factory = component_factory.getModelFactory();

      // Create a standard component adapter.  This will be used to invoke commands on other components.
      standardComponentAdapter = new StandardComponentAdapter( model_factory );

      // Create a uci for this component
      UCI client_uci = model_factory.newUCI( my_uci );

      // Create a component declaration for this component.  It is used to declare capabilities for the component.
      PrimaryComponentDeclaration client_declaration
        = component_factory.createPrimaryComponentDeclaration( client_uci );

      MY_LOGGER.info( "Query: " + query );
      // Setup a subscription to receive everything.  While setting up this subscription,
      // we will also setup what to do with the incoming messages from the subscription
      final ComponentBehavior behavior = createSubscriptionBehavior( model_factory , query );

      // Declare the subscription behavior
      client_declaration.declare( behavior );

      // Create a primary component using the component declaration
      client_component = component_factory.createPrimaryComponent( client_declaration );

      // Create a uci for the controller you are connecting to.
      UCI server_uci = model_factory.newUCI( controllerUCI );

      // Create an endpoint descriptor for the controller you want to connect to.
      server_endpoint = new EndpointDescriptor(
        InetAddresses.forString( controller_ip ) ,
        Integer.parseInt( controller_port ) ,
        server_uci
      );

      // Connect the client to the server
      final ConnectionMonitor monitor = client_component.addClientToEndpoint( server_endpoint );

      // Add a connection monitor to see what is happening.
      monitor.addLifeCycleListener((LifeCyclePhase prev , LifeCyclePhase next) ->
      {
        writeLogInfo( prev.getPhaseID() + " => " + next.getPhaseID() );
        if ( ( !client_connected.get() ) &&
               ( next.getPhaseID().equals( LifeCyclePhase.PhaseID.OPERATION ) ) )
        {
          client_connected.set( true );
          theFrame.setConnActValue( "Connected" );
          theFrame.writeToStatus( "Connected" );
          addText( "Connected." );
        }
      });
    }
    catch ( ComponentFactoryException e )
    {
      addText( "Problem initializing the component factory: " + e.getLocalizedMessage() );
      MY_LOGGER.error( "Exception initializing the component factor." , e );
    }
    catch ( DataQueryParserException e )
    {
      Optional<String> reason = e.getStartIndex().transform( (Integer index) -> "near index " + index.toString() );
      MY_LOGGER.error( "Exception initializing the subscription." , e );
      addText( "Error while parsing subscription " + reason.or( "at unknown location." ) );
      writeLogError( "Exception while parsing subscription " + reason.or( "at unknown location." ) );
    }
    catch ( NumberFormatException ex )
    {
      MY_LOGGER.error( "Exception processing message" , ex );
      addText( "Exception processing message: " + ex.getLocalizedMessage() );
    }
  }

  /**
   * Write text to the GUI window.
   *
   * @param text the string to be written
   */
  private void addText( String text )
  {
    if ( ( text != null ) && ( !text.isEmpty() ) )
    {
      theFrame.writeToViewingArea( text );
    }
    else
    {
      theFrame.writeToViewingArea( " " );
    }
  }

  /**
   * Creates subscription behavior that will subscribe to data as well as process the incoming messages
   *
   * @param model_factory model factory to use to create data structures
   * @param subscription  subscription to send to live data service
   *
   * @return component behavior that subscribes to live data
   * 
   * @throws gov.isa.spi.DataQueryParserException if the subscription parse fails
   */
  private ComponentBehavior createSubscriptionBehavior(
    final ModelFactory model_factory ,
    String subscription
  ) throws DataQueryParserException
  {

    // Create a data query parser manager that will give us a provider
    final DataQueryParserManager manager = new DataQueryParserManager();

    // Get the data query parser provider, which will give us a data query parser
    final DataQueryParserProvider provider = manager.getDefaultProvider();

    // Get the parser, which will give us a data query once we pass it the subscription
    final DataQueryParser parser = provider.createDataQueryParser( model_factory );

    // Get the data query, which is what we can pass to the live data engine
    final DataQuery data_query = parser.parseLanguage( subscription );

    // Create the component behavior.  Once it is initialized with the Behavior Controls,
    // it will submit the subscriptions to the subscription manager (live data engine).
    //
    // Once the subscription is submitted, it will start to receive messages in the SubscriptionReceiver.
    // We will set that up to send messages to any components that have a ping or pong commands.
    return new AbstractComponentBehavior()
    {
      @Override
      public void initialize( final BehaviorControls controls )
      {
        behaviorControls = controls;
//        LinkedBlockingQueue<Runnable> aQueue = new LinkedBlockingQueue<>();
//        JCADReportProcessor processor = new JCADReportProcessor( outFile );
//        ThreadPoolExecutor executor = new ThreadPoolExecutor( 2 , 8 , 30L , TimeUnit.SECONDS , aQueue );
//        executor.execute( processor );
        sub_manager = controls.getSubscriptionManager();
        subscriptionID = sub_manager
          .submitSubscription( "JHRMRecorder" , // unique name for subscription
                               data_query ,
                               (SubscriptionID subscriptionID , Message message , Header header , MetaData meta) ->
                             {
                               if ( shutdownFlag.get() )
                               {
                                 return;
                               }
                               routerQueue.insertLast( new RouterDataQueueEntry( IPCQueueEventType.IPC_NEW ,
                                                                                     message , header , meta ) );

                             } , true ); // persist this subscription until this component is shut down.
      }
    };
  }

  /**
   * Round off a double value
   *
   * @param value the value to be rounded
   *
   * @return the rounded value
   */
  private double roundDouble( Double value )
  {
    return Math.round( value * 100.0 ) / 100.0;
  }

  /**
   * Write a string to the log at INFO level
   *
   * @param info the string to be written
   */
  private void writeLogInfo( String info )
  {
    MY_LOGGER.info( info );
  }

  /**
   * Write a string to the log at the ERROR level
   *
   * @param info the string to be written
   */
  private void writeLogError( String info )
  {
    MY_LOGGER.error( info );
  }

  /**
   * Handle life cycle changes
   *
   * @param monitor - ConnectionMonitor
   */
//  private void addConnectionLogger( ConnectionMonitor monitor )
//  {
//    monitor.addLifeCycleListener( new LifeCycleListener()
//    {
//      @Override
//      public void onLifeCycleChange( LifeCyclePhase prev , LifeCyclePhase next )
//      {
//        writeLogInfo( prev.getPhaseID() + " => " + next.getPhaseID() );
//      }
//    } );
//  }
}
